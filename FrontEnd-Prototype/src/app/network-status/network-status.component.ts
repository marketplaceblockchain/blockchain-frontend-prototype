import { Component, OnInit } from '@angular/core';
import { BlockChainService } from '../block-chain.service'
import { status } from "../interfaceDef"

@Component({
  selector: 'app-network-status',
  templateUrl: './network-status.component.html',
  styleUrls: ['./network-status.component.css']
})
export class NetworkStatusComponent implements OnInit {
  netStat:status;

  constructor(private service:BlockChainService) { }

  ngOnInit() {
    this.getStatus();
  }
  getStatus():void{
    this.service.getStatus().subscribe(status => this.netStat = status);
  }
  
}
