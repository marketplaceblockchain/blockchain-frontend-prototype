export interface status{
    accounts:string[]
    mining:boolean
}
export interface block{
    hash:string
    difficulty:string
    miner:string
}

export interface Contract{
    Options
}

export interface Options{
    address: string
    Json_interface:JSON_INTERFACE[]
}

export interface JSON_INTERFACE{
    constant: boolean,
    inputs: [
        {
            name: string,
            type: number
        }
    ],
    name: string,
    outputs: string,
    payable: boolean,
    stateMutability: string,
    type: string,
    signature: string
}
export interface request{
    address:string
    abi
}
export interface product{
    name:string
    value:number
}