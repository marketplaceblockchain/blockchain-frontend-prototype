import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router'
import { CommonModule } from '@angular/common';
import { MainViewComponent } from './main-view/main-view.component';
import { BlockDataComponent } from './block-data/block-data.component';
import { SmartContractComponent } from './smart-contract/smart-contract.component'
import { NetworkStatusComponent } from './network-status/network-status.component'
import { ProductViewComponent } from './product-view/product-view.component';
import { AccBalanceComponent } from './acc-balance/acc-balance.component';

const routes: Routes = [
  {path: '', redirectTo: '/MainView', pathMatch: 'full' },
  {path: 'MainView',component: MainViewComponent},
  {path: 'BlockData',component: BlockDataComponent},
  {path: 'SmartContract',component: SmartContractComponent},
  {path: 'netStatus',component: NetworkStatusComponent},
  {path: 'Producto',component: ProductViewComponent},
  {path: 'Balance',component: AccBalanceComponent}
]


@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports:[
    RouterModule
  ],
  declarations: [
  ]
})
export class AppRoutingModule { }
