import { Component, OnInit } from '@angular/core';
import { BlockChainService } from '../block-chain.service';
import { Contract,request } from '../interfaceDef';

@Component({
  selector: 'app-smart-contract',
  templateUrl: './smart-contract.component.html',
  styleUrls: ['./smart-contract.component.css']
})
export class SmartContractComponent implements OnInit {
  contrato:JSON;
  id: request = {
    address:"",
    abi : [ { "constant": false, "inputs": [ { "name": "quantity", "type": "uint256" } ], "name": "addAsset", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "addr", "type": "address" } ], "name": "removeAdmin", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "addr", "type": "address" } ], "name": "isAdmin", "outputs": [ { "name": "result", "type": "bool", "value": false } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "kill", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "addr", "type": "address" } ], "name": "addAdmin", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "customer", "type": "address" }, { "name": "quantity", "type": "uint256" } ], "name": "buy", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [ { "name": "addr", "type": "address" } ], "name": "getAddressStock", "outputs": [ { "name": "stock", "type": "uint256", "value": "0" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getStock", "outputs": [ { "name": "stock", "type": "uint256", "value": "99999999" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "from", "type": "address" }, { "indexed": false, "name": "to", "type": "address" }, { "indexed": false, "name": "quantity", "type": "uint256" } ], "name": "Transaction", "type": "event" } ]
  }
  constructor(private service: BlockChainService) { }
  ngOnInit() {
  }

  getContract():void{
    this.service.getContract(this.id).subscribe(derp =>{this.contrato = derp
    console.log(this.contrato)});
  }
}
