import { Component, OnInit } from '@angular/core';
import { BlockChainService } from '../block-chain.service';
import { Contract,request,product } from '../interfaceDef';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {
  producto:product = {
    name:"Pasta de Dientes",
    value:5000
  }
  password:string;
  private account="0xe6271b39f4A1dE18A5Ae68E6fc73796948dca20f";

  constructor(
    private service:BlockChainService
  ) { }

  ngOnInit() {
  }

  buySwag():void{
    this.service.getProduct();
  }
}