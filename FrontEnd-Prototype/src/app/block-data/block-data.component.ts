import { Component, OnInit } from '@angular/core';
import { BlockChainService } from '../block-chain.service'
import { block } from "../interfaceDef"

@Component({
  selector: 'app-block-data',
  templateUrl: './block-data.component.html',
  styleUrls: ['./block-data.component.css']
})
export class BlockDataComponent implements OnInit {
  blockId:number;
  specBlock:block;


  constructor(private service:BlockChainService ) { }

  ngOnInit() {
  }

  getBlockData():void{
    this.service.getBlock(this.blockId).subscribe(blockData =>this.specBlock = blockData);
  }

}
