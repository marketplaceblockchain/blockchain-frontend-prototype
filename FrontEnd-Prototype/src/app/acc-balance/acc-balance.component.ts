import { Component, OnInit } from '@angular/core';
import { BlockChainService } from '../block-chain.service';
@Component({
  selector: 'app-acc-balance',
  templateUrl: './acc-balance.component.html',
  styleUrls: ['./acc-balance.component.css']
})
export class AccBalanceComponent implements OnInit {
  ident: string;
  saldo: JSON;

  constructor(private service:BlockChainService) { }

  ngOnInit() {
  }

  getSaldo(ident):void{
    this.service.getBalance(this.ident).subscribe(result=>this.saldo = result);
  }
}
