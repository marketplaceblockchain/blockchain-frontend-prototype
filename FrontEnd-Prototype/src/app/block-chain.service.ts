import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { status,block,Contract,request } from "./interfaceDef"

@Injectable()
export class BlockChainService {
  private loadBlock = "http://10.10.1.209:3000/eth/block/";
  private loadStatus = "http://10.10.1.209:3000/eth";
  private loadContract = "http://10.10.1.209:3000/eth/contract";
  private loadBalance = "http://10.10.1.209:3000/eth/marketManager/balance/"
  private buyProduct = "http://10.10.1.209:3000/eth/marketManager/buy/pastaDeDientes";

  constructor(private http:HttpClient) { }

  getBlock(id:number): Observable<block>{
    return this.http.get<block>(this.loadBlock+id);
  }

  getStatus(): Observable<status>{
    return this.http.get<status>(this.loadStatus);
  }
  getContract(address:request): Observable<JSON>{
    return this.http.post<JSON>(this.loadContract,JSON.parse(JSON.stringify(address)),{headers:{'Content-Type':'application/json'}});
  }
  getBalance(ident:string):Observable<JSON>{
    return this.http.get<JSON>(this.loadBalance+ident);
  }
  getProduct(){
    this.http.get(this.buyProduct).subscribe();
  }
}
